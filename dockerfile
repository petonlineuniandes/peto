FROM    centos:centos6
RUN     yum install -y epel-release
RUN     yum install -y nodejs npm mongodb
RUN     npm install -g mongodb
RUN     npm install -g assert
RUN     npm install -g amqplib
RUN     npm install -g seneca
RUN     npm install -g express
RUN     npm install -g body-parser
COPY package.json /src/package.json
RUN cd /src; npm link body-parser; npm link express; npm link seneca; npm link mongodb; npm link amqplib; npm link assert; npm install --production
COPY . /src
EXPOSE  8080
CMD ["node", "/src/server-index.js"]
