var amqp = require('amqplib/callback_api');

amqp.connect('amqp://cola', function(err, conn) {
  conn.createChannel(function(err, ch) {
    var q = 'consulta';

    ch.assertQueue(q, {durable: false});
    var consulta1 = '{consulta: {estampa_tiempo: "20160228 12:35:25", mascota: "tiburon", nombreTienda: "PETS for Everyone", idTienda: "1004"}}';
    var consulta2 = '{consulta: {estampa_tiempo: "20160228 12:35:26", mascota: "cocodrilo", nombreTienda: "My PET", idTienda: "1005"}}';
    var consulta3 = '{consulta: {estampa_tiempo: "20160228 12:35:27", mascota: "leon", nombreTienda: "The PET", idTienda: "1006"}}';


    ch.sendToQueue(q, new Buffer(consulta1));
    console.log(" [x] Envio " + consulta1);
    ch.sendToQueue(q, new Buffer(consulta2));
    console.log(" [x] Envio " + consulta2);
    ch.sendToQueue(q, new Buffer(consulta3));
    console.log(" [x] Envio " + consulta3);

    setTimeout(function() { conn.close(); process.exit(0) }, 500);
  });
});

amqp.connect('amqp://cola', function(err, conn) {
  conn.createChannel(function(err, ch) {
    var q = 'consulta';

    ch.assertQueue(q, {durable: false});
    var consulta = '';
    var fecha= new Date();

    for (var i = 0; i < 100; i++) {
        consulta = '{consulta: {estampa_tiempo: "'+fecha.toISOString()+'", mascota: "mascota_'+i+'", nombreTienda: "Tienda_'+i+'", idTienda: "1006'+i+'"}}';
        ch.sendToQueue(q, new Buffer(consulta));
        console.log(" [x] Envio " + consulta);
    }
    setTimeout(function() { conn.close(); process.exit(0) }, 500);
  });
});

