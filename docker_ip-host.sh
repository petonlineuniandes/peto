#!/bin/sh

IP_COLA=`docker exec 3d3c88bb84c9 ip addr | grep eth | grep inet | awk '{ print $2 }' | cut -d "/" -f 1`;

echo $IP_COLA" "cola >> /etc/hosts
