var amqp = require('amqplib/callback_api');

amqp.connect('amqp://cola', function(err, conn) {
  conn.createChannel(function(err, ch) {
    var q = 'consulta';

    ch.assertQueue(q, {durable: false});

    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
    ch.consume(q, function(msg) {
        console.log(" [x] Recibido %s", msg.content.toString());
    }, {noAck: true});

//    setTimeout(function() { conn.close(); process.exit(0) }, 500);
  });
});

