var mongo = require('mongodb'), Server = mongo.Server, Db = mongo.Db;
var server = new Server('localhost', 27017, { auto_reconnect: true });
var db = new Db('test', server);
var onErr = function(err, callback) {
  db.close();
  callback(err);
};


module.exports = function service(options) {

  this.add({ type: 'consultaspeto', function: 'consultar' }, consultar)

  function consultar(msg, respond) {
    var vars = msg.vars;
    var Filter = vars.filtro;



    respond( null, { respond: 'Estos son los datos que estoy consultado desde la base de datos Mongo' });
  }

}


exports.teamlist = function(gname, callback) {
  db.open(function(err, db) {
    if (!err) {
      db.collection('terceros', function(err, collection) {
        if (!err) {
          collection.find({
            'Nombre_Empresa': gname
          }).toArray(function(err, docs) {
            if (!err) {
              db.close();
              var intCount = docs.length;
              if (intCount > 0) {
                var strJson = "";
                for (var i = 0; i < intCount;) {
                  strJson += '{"Ciudad":"' + docs[i].Ciudad + '"}'
                  i = i + 1;
                  if (i < intCount) {
                    strJson += ',';
                  }
                }
                strJson = '{"GroupName":"' + gname + '","count":' + intCount + ',"teams":[' + strJson + "]}"
                callback("", JSON.parse(strJson));
              }
            } else {
              onErr(err, callback);
            }
          }); //end collection.find
        } else {
          onErr(err, callback);
        }
      }); //end db.collection
    } else {
      onErr(err, callback);
    }
  }); // end db.open
};
